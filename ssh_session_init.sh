# scp -i ~/.ssh/id_ifb ssh_session_init.sh ssh_session_adduser.sh 2020_participants_mail.txt ubuntu@134.158.247.127:
# ssh -i ~/.ssh/id_ifb ubuntu@134.158.247.127
# bash ssh_session_init.sh 2020_participants_mail.txt

cat $1 | sed -e "s/@[^,]*[,]*//g" | tr ' ' '\n' | awk '{system("bash ssh_session_adduser.sh "$0)}'
sudo sed -i 's|PasswordAuthentication no|PasswordAuthentication yes|' /etc/ssh/sshd_config
sudo systemctl restart sshd
