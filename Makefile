all: public/index.html \
	public/github-pandoc.css \
	public/1_understanding_a_computer.html \
	public/2_using_the_ifb_cloud.html \
	public/3_first_steps_in_a_terminal.html \
	public/4_unix_file_system.html \
	public/5_users_and_rights.html \
	public/6_unix_processes.html \
	public/7_streams_and_pipes.html \
	public/8_text_manipulation.html \
	public/9_batch_processing.html \
	public/10_network_and_ssh.html \
	public/11_install_system_programs.html \
	public/12_virtualization.html

public/github-pandoc.css: github-pandoc.css
	cp github-pandoc.css public/github-pandoc.css
	cp -R img public/
	cp *.Rmd public/
	cp -R www public/

public/index.html: index.md github-pandoc.css
	pandoc -s -c github-pandoc.css index.md -o public/index.html

public/1_understanding_a_computer.html: 1_understanding_a_computer.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("1_understanding_a_computer.Rmd")'

public/2_using_the_ifb_cloud.html: 2_using_the_ifb_cloud.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("2_using_the_ifb_cloud.Rmd")'

public/3_first_steps_in_a_terminal.html: 3_first_steps_in_a_terminal.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("3_first_steps_in_a_terminal.Rmd")'

public/4_unix_file_system.html: 4_unix_file_system.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("4_unix_file_system.Rmd")'

public/5_users_and_rights.html: 5_users_and_rights.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("5_users_and_rights.Rmd")'

public/6_unix_processes.html: 6_unix_processes.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("6_unix_processes.Rmd")'

public/7_streams_and_pipes.html: 7_streams_and_pipes.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("7_streams_and_pipes.Rmd")'

public/8_text_manipulation.html: 8_text_manipulation.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("8_text_manipulation.Rmd")'

public/9_batch_processing.html: 9_batch_processing.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("9_batch_processing.Rmd")'

public/10_network_and_ssh.html: 10_network_and_ssh.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("10_network_and_ssh.Rmd")'

public/11_install_system_programs.html: 11_install_system_programs.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("11_install_system_programs.Rmd")'

public/12_virtualization.html: 12_virtualization.Rmd public/github-pandoc.css
	cd public && Rscript -e 'rmarkdown::render("12_virtualization.Rmd")'
