---
title: #  Unix / command line training course
---

# Unix / command line training course

1. [Understanding a computer](./1_understanding_a_computer.html)
2. [Using the IFB cloud](./2_using_the_ifb_cloud.html)
3. [First steps in a terminal](./3_first_steps_in_a_terminal.html)
4. [The Unix file system.](./4_unix_file_system.html)
5. [Users and rights](./5_users_and_rights.html)
6. [Unix processes](./6_unix_processes.html)
7. [Streams and pipes](./7_streams_and_pipes.html)
8. [Text manipulation](./8_text_manipulation.html)
9. [Batch processing](./9_batch_processing.html)
10. [Network and ssh](./10_network_and_ssh.html)
11. [Install system-wide programs](./11_install_system_programs.html)
12. [Virtualization](./12_virtualization.html)


