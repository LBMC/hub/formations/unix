# UNIX training course

[![cc_by_sa](./img/cc_by_sa.png)](http://creativecommons.org/licenses/by-sa/4.0/)

All the materials are accessible at the following url:

[https://can.gitbiopages.ens-lyon.fr/unix-command-line/](https://can.gitbiopages.ens-lyon.fr/unix-command-line/)

You can join us on the dedicated matrix channel (ask [laurent.modolo@ens-lyon.fr](mailto:laurent.modolo@ens-lyon.fr))
