#!/bin/bash

# This script is executed on the virtual machine during the *Deployment* phase.
# It is used to apply parameters specific to the current deployment.
# It is executed secondly during a cloud deployement in IFB-Biosphere, after the *Installation* phase.

USER_LOGIN=etudiant
USER_PASSWORD=$( openssl rand -hex 12 )

useradd -m -s /bin/bash -g users -G adm,docker,dialout,cdrom,floppy,audio,dip,video,plugdev,netdev ${USER_LOGIN}
cp /etc/skel/.* /home/${USER_LOGIN}/

passwd ${USER_LOGIN} << EOF
${USER_PASSWORD}
${USER_PASSWORD}
EOF

HOST_NAME=$( ss-get --timeout=3 hostname )
HTTP_ENDP="https://$HOST_NAME"
ss-set url.service "${HTTP_ENDP}"
ss-set ss:url.service "[HTTPS]$HTTP_ENDP,[LOGIN]$USER_LOGIN,[PASSWORD]$USER_PASSWORD"
